package com.messenger.inwady.messenger.controller;


import android.app.Activity;
import android.util.Log;

import com.messenger.inwady.messenger.model.utils.Message;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;

public class EventsConroller extends MainController {

    public static String actionEnter = null;
    public static String actionLeave = null;
    public static String actionMessage = null;

    public static String my_nickname = null;
    static {
        actionEnter = "ev_enter";
        actionLeave = "ev_leave";
        actionMessage = "ev_message";
    }

    public EventsConroller(Activity act, GenerateEvent e) {
        super(act, e);
    }

    @Override
    public void startController() {
        link.putEvent(actionEnter, event.getPost());
        link.putEvent(actionLeave, event.getPost());
        link.putEvent(actionMessage, event.getPost());
    }

    @Override
    public void onDestroy() {
        link.removeEvent(actionEnter);
        link.removeEvent(actionLeave);
        link.removeEvent(actionMessage);
    }

    public static Message genAnswer(String action, boolean my, String nick, String body, Pack pack) {
        if(action.equals("ev_message")) {
            if(body == null) return new Message(true, nick, "Error!");
            return  new Message(my, nick, body, /* FOR U */ /* (my) ? null : */ pack.data.get("from"));
        }else if(action.equals("ev_enter")) {
            if(my_nickname == null) my_nickname = nick;
            return new Message(my, nick, "Enter the room.", /* FOR U */ /* (my) ? null : */ pack.data.get("uid"));
        }else if(action.equals("ev_leave")) {
            return new Message(my, nick, "Leave the room.", /* FOR U */ /* (my) ? null : */ pack.data.get("uid"));
        }
        return new Message(true, "You", "error!", null);
    }

}