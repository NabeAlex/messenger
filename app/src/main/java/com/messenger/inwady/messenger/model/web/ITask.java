package com.messenger.inwady.messenger.model.web;

import android.os.Handler;

import java.net.Socket;

public interface ITask {
    boolean start(Socket s, Handler handler);
    void destroyTask();
}
