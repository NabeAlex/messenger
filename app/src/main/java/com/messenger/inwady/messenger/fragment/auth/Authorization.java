package com.messenger.inwady.messenger.fragment.auth;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.messenger.inwady.messenger.Login;
import com.messenger.inwady.messenger.Main;
import com.messenger.inwady.messenger.R;
import com.messenger.inwady.messenger.controller.AuthController;
import com.messenger.inwady.messenger.model.App;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;
import com.messenger.inwady.messenger.ui.DefaultEditText;
import com.messenger.inwady.messenger.utils.MD5;
import com.messenger.inwady.messenger.utils.MessengerSharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.zip.Inflater;

public class Authorization extends Fragment {
    public Fragment me = null;
    private App app = null;
    private Login act = null;

    public LayoutInflater inflater = null;
    public void setInflater(LayoutInflater i) {
        inflater = i;
    }

    LinearLayout l = null;
    Button logButton = null;

    private AlertDialog dialogLoading = null;
    private AlertDialog dialogNick = null;
    AlertDialog.Builder builder = null;

    EditText pasEd;
    AutoCompleteTextView logEd;
    AutoCompleteTextView nick;
    Button confirm;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_login, container, false);
        setInflater(inflater);

        app = App.getInstance();
        act = (Login)getActivity();

        me = this;

        setEvents();

        l = (LinearLayout) view.findViewById(R.id.login_main);

        logButton = (Button) view.findViewById(R.id.sign_up);

        pasEd = (EditText) view.findViewById(R.id.password);
        logEd = (AutoCompleteTextView) view.findViewById(R.id.email);

        pasEd.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        app.authController = new AuthController(getActivity(), app.lin);

        if(App.mprefs.isValid()) {
            auto = true;
            l.setVisibility(View.INVISIBLE);
            app.authController.setPack(App.mprefs.getLogin(), App.mprefs.getPassword(), false);
            app.authController.startController();
        }

        logButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        return view;
    }

    private boolean auto = false;
    private String login = null;
    private String nickname = null;
    private String password = null;

    private void attemptLogin() {
        logEd.setError(null);
        pasEd.setError(null);

        login = logEd.getText().toString();
        password = pasEd.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            pasEd.setError(getString(R.string.error_invalid_password));
            focusView = pasEd;
            cancel = true;
        }

        if (TextUtils.isEmpty(login)) {
            logEd.setError(getString(R.string.error_field_required));
            focusView = logEd;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {

            builder = new AlertDialog.Builder(getActivity());


            builder.setView(inflater.inflate(R.layout.dialog_loading, null));

            builder.setCancelable(false);

            dialogLoading = builder.create();
            dialogLoading.show();

            app.authController.setEvent(app.lin);
            app.authController.setPack(login, password);
            app.authController.startController();

        }
        l.setVisibility(View.VISIBLE);
    }

    public void login(int status, Pack pack) {
        if(status == 7) {
            View v = inflater.inflate(R.layout.dialog_nick, null);
            setRegister(v);
            builder = new AlertDialog.Builder(act);
            builder.setView(v);
            dialogNick = builder.create();

            if(!((Activity) act).isFinishing())
            {
                dialogNick.show();
            }
        }else  if(status == 2) {

        }else if(status == 0){
            if(pack.data.containsKey("sid") && pack.data.containsKey("cid") && ((login != null
                                                                            && password != null) || auto)) {
                String sid = pack.data.get("sid");
                String cid = pack.data.get("cid");

                app.setAuth(sid, cid);

                if(auto) {
                    app.login = App.mprefs.getLogin();
                    app.nickname = (App.mprefs.getNick().equals("")) ? null : App.mprefs.getNick();
                }else{
                    app.login = login;
                    if(nickname != null) {
                        app.nickname = nickname;
                        App.mprefs.setSharedPrefs(login, MD5.generate(password), nickname);
                    } else App.mprefs.setSharedPrefs(login, MD5.generate(password));
                }

                Intent intent = new Intent(act, Main.class);
                intent.setFlags(Intent.FILL_IN_ACTION);
                act.startActivity(intent);
                act.finish();
                return;
            }else{
                App.Toast(act, "Sorry, maybe later.");
            }

        }
        auto = false;
    }

    private void attemptNick() {
        nickname = nick.getText().toString();
        login = logEd.getText().toString();
        password = pasEd.getText().toString();
        app.authController.setPack(login, password, nickname);
        app.authController.setEvent(app.reg);
        app.authController.startController();
    }

    private void setRegister(View v) {
        nick = (AutoCompleteTextView)v.findViewById(R.id.nickname);
        confirm = (Button)v.findViewById(R.id.nick_confirm);

        nick.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                attemptNick();
                return false;
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptNick();
                return;
            }
        });
    }

    private void setEvents() {
        app.lin = new GenerateEvent(new Handler() {
            public void handleMessage(Message msg) {
                if(dialogLoading != null && dialogLoading.isShowing()) dialogLoading.dismiss();
                String s = Pack.parseMessageJson(msg);
                Pack pack = new Pack(s);
                login(Integer.parseInt(pack.data.get("status")), pack);
            }
        });

        app.reg = new GenerateEvent(new Handler() {
            public void handleMessage(Message msg) {

                String s = Pack.parseMessageJson(msg);
                Pack pack = new Pack(s);

                switch (Pack.statusError(pack)) {
                    case 0:
                        if(dialogNick != null && dialogNick.isShowing()) dialogNick.dismiss();
                        //app.authController.startController();
                        break;
                    case  5:
                        App.Toast(getActivity(), "Please, chose other Nickname");
                    default:
                        App.Toast(getActivity(), "Oh, sorry!"); // debug
                }
            }
        });
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

}