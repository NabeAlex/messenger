package com.messenger.inwady.messenger.model.utils;

public class Message {
    public boolean my;
    public String user;
    public String message;

    private String uid;
    public String getUser() {
        return  uid;
    }

    public Message(boolean my, String User, String message) {
        this.my = my;
        this.message = message;
        this.user = User;
        this.uid = null;
    }

    public Message(boolean my, String User, String message, String uid) {
        this.my = my;
        this.message = message;
        this.user = User;
        this.uid = uid;
    }
}