package com.messenger.inwady.messenger.model.web;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.messenger.inwady.messenger.data.Resource;
import com.messenger.inwady.messenger.model.App;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class ConnectTask implements ITask {
    public App app = null;

    private Socket socket = null;

    public ConnectTask() {}

    private Task task = null;

    @Override
    public boolean start(Socket s, Handler handler) {
        if(socket != null) return false;
        socket = s;

        task = new Task();
        task.h = handler;
        task.execute();
        return true;
    }

    @Override
    public void destroyTask() {}

    private class Task extends AsyncTask<Void, Void, Void> {
        public Handler h = null;

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                socket = new Socket(Resource.ip, Integer.parseInt(Resource.port));
                socket.setSoTimeout(500);

                socket.setKeepAlive(true);
            } catch (SocketException e) {
                e.printStackTrace();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            app.mService.setSocket(socket);
            if(h != null) h.handleMessage(new Message());
        }
    }


}