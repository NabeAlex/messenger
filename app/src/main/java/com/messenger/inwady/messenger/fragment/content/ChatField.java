package com.messenger.inwady.messenger.fragment.content;

import android.app.Fragment;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.messenger.inwady.messenger.Main;
import com.messenger.inwady.messenger.R;
import com.messenger.inwady.messenger.controller.ChatsController;
import com.messenger.inwady.messenger.controller.EnterChatController;
import com.messenger.inwady.messenger.controller.EventsConroller;
import com.messenger.inwady.messenger.controller.LeaveChatController;
import com.messenger.inwady.messenger.controller.SendMessageController;
import com.messenger.inwady.messenger.model.App;
import com.messenger.inwady.messenger.model.utils.Message;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;
import com.messenger.inwady.messenger.ui.adapter.MessageArrayAdapter;

import org.json.JSONException;


public class ChatField extends Fragment implements View.OnClickListener {
    App app = null;

    private MessageArrayAdapter chatAdapter;

    /*** Controllers ***/

    GenerateEvent enterEvent;
    private EnterChatController enterController = null;

    GenerateEvent leaveEvent;
    private LeaveChatController leaveController = null;

    GenerateEvent sendEvent;
    private SendMessageController sendController = null;

    GenerateEvent eventEvent; /* ^_^ */
    private EventsConroller eventConroller = null;

    /*******************/

    private ListView messagesList;
    private EditText textField;
    private Button sendButton;
    private ProgressBar progressBar;

    private boolean side = false;

    public String nameChat = null;
    public String chid = null;

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    private static boolean progressRun = true;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        progressRun = true;

        app = App.getInstance();

        Bundle bundle = this.getArguments();
        chid = bundle.getString("chid");
        nameChat = bundle.getString("name");

        chatAdapter = new MessageArrayAdapter(getActivity().getApplicationContext(), R.layout.chat_list_item);
        setEvents();

        eventConroller = new EventsConroller(getActivity(), eventEvent);
        eventConroller.startController();

        enterController = new EnterChatController(getActivity(), enterEvent);
        enterController.setPack(app.sid, app.cid, chid);
        enterController.startController();

        leaveController = new LeaveChatController(getActivity(), leaveEvent);
        leaveController.setPack(app.sid, app.cid, chid);

        sendController = new SendMessageController(getActivity(), sendEvent);

    }

    //private static
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.screen_chat, container, false);
        App.now = App.Screen.chat;

        ((Main) getActivity()).setToolbar(nameChat);

        //** TO **//


        sendButton = (Button) view.findViewById(R.id.buttonSend);

        messagesList = (ListView) view.findViewById(R.id.listView1);


        messagesList.setAdapter(chatAdapter);

        messagesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long l) {
                Message message = (Message) parent.getItemAtPosition(position);
                if (message.getUser() != null) {
                    Bundle b = new Bundle();
                    b.putString("uid", message.getUser());
                    UserInfo info = new UserInfo();
                    info.setArguments(b);
                    ((Main) getActivity()).updateChatFragment(info);
                }
                /*
                Toast:
                    E/rsC++: RS CPP error (masked by previous error): Allocation creation failed
                    E/rsC++: RS CPP error (masked by previous error): Allocation creation failed
                    Fatal signal 11 (SIGSEGV), code 1, fault addr 0x28 in tid 4521 (RenderThread)
                 */
            }
        });

        textField = (EditText) view.findViewById(R.id.chatText);
        progressBar = (ProgressBar) view.findViewById(R.id.progressChatBar);

        if(!progressRun) {
            progressBar.setVisibility(View.INVISIBLE);
        } else {

            messagesList.setEnabled(false);
            textField.setEnabled(false);

            sendButton.setEnabled(false);
        }
        sendButton.setOnClickListener(this);

        messagesList.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);

        chatAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                messagesList.setSelection(chatAdapter.getCount() - 1);
            }
        });

        return view;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        eventConroller.onDestroy();
    }

    private void setEvents() {
        enterEvent = new GenerateEvent(new Handler() {
            public void handleMessage(android.os.Message msg) {
                sendButton.setEnabled(true);
                if(progressRun)
                    progressBar.setVisibility(View.INVISIBLE);
                progressRun = false;
                textField.setEnabled(true);
                messagesList.setEnabled(true);
            }
        });

        leaveEvent = new GenerateEvent(new Handler() {
            public void handleMessage(android.os.Message msg) {

            }
        });

        /*
          Note:
            WhatsUp status
        */
        sendEvent = new GenerateEvent(new Handler() {
            public void handleMessage(android.os.Message msg) {

            }
        }, new Handler() {
            public void handleMessage(android.os.Message msg) {
                textField.setText("");
                sendButton.setEnabled(true);
            }
        });

        eventEvent = new GenerateEvent(new Handler() {
            public void handleMessage(android.os.Message msg) {
                Pack pack = new Pack(Pack.parseMessageJson(msg));
                String body = (pack.data.containsKey("body")) ? pack.data.get("body") : null;
                if(app.nickname != null)
                    chatAdapter.add(EventsConroller.genAnswer(pack.action, app.nickname.equals(pack.data.get("nick")),
                                                          pack.data.get("nick"), body, pack));
                else
                    chatAdapter.add(EventsConroller.genAnswer(pack.action, true,
                                                              pack.data.get("nick"), body, pack));
                if(EventsConroller.my_nickname != null) app.nickname = EventsConroller.my_nickname;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonSend:
                if(textField != null && !textField.getText().toString().equals("")) {
                    String body = textField.getText().toString();
                    sendController.setPack(app.sid, app.cid, chid, body);
                    sendController.startController();
                    sendButton.setEnabled(false);
                }
        }
    }

    public void leaveChat() {
        leaveController.startController();
    }

}