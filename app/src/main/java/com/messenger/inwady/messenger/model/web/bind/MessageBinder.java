package com.messenger.inwady.messenger.model.web.bind;

import android.os.Binder;

import com.messenger.inwady.messenger.MessageService;

public class MessageBinder extends Binder {

    private MessageService ms = null;

    public MessageBinder(MessageService ms) {
        this.ms = ms;
    }

    public MessageService getService() {
        return ms;
    }
}
