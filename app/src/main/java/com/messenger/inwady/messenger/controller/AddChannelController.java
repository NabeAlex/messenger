package com.messenger.inwady.messenger.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Handler;
import android.text.Layout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;

import com.messenger.inwady.messenger.Main;
import com.messenger.inwady.messenger.R;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.zip.Inflater;

public class AddChannelController extends MainController {
    public static String action = "createchannel";

    public AddChannelController(Activity act, GenerateEvent event) {
        super(act, event);
    }

    public void setPack(String sid, String cid) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("sid", sid);
        map.put("cid", cid);
        pack = Pack.genDefaultPack(action, map);
    }

    /* UI */

    private AlertDialog dialog = null;
    private AlertDialog.Builder builder = null;
    private LayoutInflater inflater = null;
    private View v = null;

    private AutoCompleteTextView editName;
    private AutoCompleteTextView editDescr;
    private Button button;
    private TextView error;

    public AddChannelController setAlert(Activity act, LayoutInflater i) {
        inflater = i;

        builder = new AlertDialog.Builder(act);

        v = inflater.inflate(R.layout.dialog_add_channel, null);

        editName = (AutoCompleteTextView) v.findViewById(R.id.channel_name);
        editDescr = (AutoCompleteTextView) v.findViewById(R.id.channel_descr);
        button = (Button) v.findViewById(R.id.channel_confirm);

        error = (TextView) v.findViewById(R.id.channel_error);
        error.setVisibility(View.GONE);

        builder.setView(v);
        builder.setCancelable(true);

        return this;
    }

    public AddChannelController setHandlerAlert() {

        editDescr.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_NULL) {
                    sendPack();
                }
                return false;
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendPack();
            }
        });
        return this;
    }

    private void sendPack() {
        pack.data.put("name", editName.getText().toString());
        pack.data.put("descr", editDescr.getText().toString());
        startController();
    }

    public void showAlert() {
        dialog = builder.create();
        dialog.show();
    }

    public void dismissAlert() {
        if(dialog.isShowing()) dialog.dismiss();
    }

    public void throwText(String s, int c) {
        error.setTextColor(c);
        error.setVisibility(View.VISIBLE);
        error.setText(s);
    }

    /******/
}
