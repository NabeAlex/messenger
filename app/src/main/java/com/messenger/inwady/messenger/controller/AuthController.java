package com.messenger.inwady.messenger.controller;

import android.app.Activity;
import android.app.AlertDialog;
import android.util.Log;

import com.messenger.inwady.messenger.model.App;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;
import com.messenger.inwady.messenger.utils.MD5;

import java.util.HashMap;

public class AuthController extends MainController {

    public static String action = "auth";
    public static String actionRegister = "register";

    public AuthController(Activity act, GenerateEvent event, Pack pack) {
        super(act, event, pack);
    }

    public AuthController(Activity act, GenerateEvent event) {
        super(act, event);
    }

    public void setPack(String login, String pass) {
        String md5 = MD5.generate(pass);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("login", login);
        map.put("pass", md5);
        pack = Pack.genDefaultPack(AuthController.action, map);
    }

    public void setPack(String login, String pass, String nick) {
        String md5 = MD5.generate(pass);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("login", login);
        map.put("pass", md5);
        map.put("nick", nick);
        pack = Pack.genDefaultPack(actionRegister, map);
    }

    public void setPack(String login, String pass, boolean md5_bool) {
        if(md5_bool) setPack(login, pass);
        else {
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("login", login);
            map.put("pass", pass);
            pack = Pack.genDefaultPack(action, map);
        }
    }
}
