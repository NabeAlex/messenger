package com.messenger.inwady.messenger.custom;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.messenger.inwady.messenger.Main;
import com.messenger.inwady.messenger.R;
import com.messenger.inwady.messenger.controller.AuthController;
import com.messenger.inwady.messenger.controller.MainController;
import com.messenger.inwady.messenger.custom.dummy.DummyContent;
import com.messenger.inwady.messenger.fragment.content.ChatField;
import com.messenger.inwady.messenger.fragment.content.Chats;
import com.messenger.inwady.messenger.model.App;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class ChatsFragment extends Fragment implements AbsListView.OnItemClickListener {
    ChatsFragment me = null;
    View view = null;

    /*** Controllers ***/

    GenerateEvent event;
    private MainController controller = null;

    /*******************/

    private AbsListView mListView;

    private ListAdapter mAdapter;

    public static ChatsFragment newInstance() {
        ChatsFragment fragment = new ChatsFragment();
        Bundle args = new Bundle();
        return fragment;
    }

    public ChatsFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //* Server gets RANDOM list! *//

        controller = new MainController(getActivity());
        controller.setEvent(new GenerateEvent(new Handler() {
            public void handleMessage(Message msg) {
                String s = Pack.parseMessageJson(msg);
                Pack pack = new Pack(s);

                if(Pack.statusError(pack) > 0) {}
                else {
                    try {
                        JSONArray jsonArray = new JSONArray(pack.data.get("channels"));
                        JSONObject tmp;
                        for(int i = 0; i < jsonArray.length(); i++) {
                            tmp = jsonArray.getJSONObject(i);

                            // REMOVE
                            if(tmp.getString("name").equals("Tests")) {
                                DummyContent.ITEMS.add(0, new DummyContent.DummyItem(i + "",
                                        tmp.getString("name"),
                                        tmp.getInt("online"),
                                        tmp.getString("descr")).setChatId(tmp.getString("chid")));
                                continue;
                            }

                            DummyContent.addItem(new DummyContent.DummyItem(i + "",
                                    tmp.getString("name"),
                                    tmp.getInt("online"),
                                    tmp.getString("descr")).setChatId(tmp.getString("chid")));
                        }

                        mListView = (AbsListView) view.findViewById(android.R.id.list);
                        mListView.setAdapter(new ChatArrayAdapter(me.getActivity(), DummyContent.ITEMS));
                        mListView.setOnItemClickListener(me);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }));
        controller.pushRouter("channellist");
        mAdapter = new ChatArrayAdapter(getActivity(), DummyContent.ITEMS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        me = this;
        view = inflater.inflate(R.layout.fragment_chats, container, false);
        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (null != mListView) {
            DummyContent.DummyItem item = DummyContent.ITEMS.get(position);

            Fragment fragment = new ChatField();
            Bundle s = new Bundle();
            s.putString("chid", item.getChatId());
            s.putString("name", item.name);
            fragment.setArguments(s);
            Main main = (Main) getActivity();
            main.updateChatFragment(fragment);
        }
    }

    public class ChatArrayAdapter extends ArrayAdapter {
        private final Context context;
        private final List<DummyContent.DummyItem> values;

        public ChatArrayAdapter(Context context, List<DummyContent.DummyItem> values) {
            super(context, R.layout.chat_list_item, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.chat_list_item, parent, false);

            TextView textChat = (TextView) view.findViewById(R.id.chat_name);
            TextView textDesc = (TextView) view.findViewById(R.id.chat_description);

            ImageView imageView = (ImageView) view.findViewById(R.id.chat_icon);

            textChat.setText(values.get(position).name + " (" + values.get(position).online + ")");
            textDesc.setText(values.get(position).desc);
            if(values.get(position).bitmap != null) imageView.setImageBitmap(values.get(position).bitmap);
            return view;
        }
    }

}
