package com.messenger.inwady.messenger.fragment.content;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.messenger.inwady.messenger.Main;
import com.messenger.inwady.messenger.R;
import com.messenger.inwady.messenger.controller.ChatsController;
import com.messenger.inwady.messenger.controller.UserInfoController;
import com.messenger.inwady.messenger.model.App;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;

import org.w3c.dom.Text;

public class UserInfo extends Fragment  implements View.OnClickListener {
    public App app = null;
    public Fragment me = null;

    public LayoutInflater inflater = null;
    public void setInflater(LayoutInflater i) {
        inflater = i;
    }


    /*** Controllers ***/

    GenerateEvent user;
    private UserInfoController userInfoController = null;

    /*******************/

    private static View view;
    static {
        view = null;
    }

    private TextView status = null;
    private TextView nick = null;

    private String uid = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.screen_user_info,
                    container, false);
        }catch (InflateException e) {}

        app = App.getInstance();
        App.now = App.Screen.user_info;

        me = this;

        Bundle bundle = this.getArguments();
        uid = bundle.getString("uid");
        ((Main) getActivity()).setToolbar(uid);

        setEvents();

        status = (TextView)view.findViewById(R.id.info_status);
        nick = (TextView)view.findViewById(R.id.info_nick);

        userInfoController = new UserInfoController(getActivity(), user);
        userInfoController.setPack(app.sid, app.cid, uid);
        userInfoController.startController();

        return view;
    }

    private void HandleError(int status) {
        App.Toast(app, "Error!");
    }

    private void setEvents() {
        user = new GenerateEvent(new Handler() {
            public void handleMessage(Message msg) {
                Pack s = Pack.parseMessagePack(msg);
                nick.setText(
                        s.data.get("nick")
                );
                status.setText(
                        s.data.get("user_status")
                );
            }
        });
    }

    @Override
    public void onClick(View v) {

    }
}
