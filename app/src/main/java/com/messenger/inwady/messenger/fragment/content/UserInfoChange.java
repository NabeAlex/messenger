package com.messenger.inwady.messenger.fragment.content;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.messenger.inwady.messenger.R;
import com.messenger.inwady.messenger.controller.ChangeUserInfoController;
import com.messenger.inwady.messenger.controller.ChatsController;
import com.messenger.inwady.messenger.controller.UserInfoController;
import com.messenger.inwady.messenger.model.App;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;

import org.w3c.dom.Text;

public class UserInfoChange extends Fragment  implements View.OnClickListener {
    public App app = null;
    public Fragment me = null;

    public LayoutInflater inflater = null;
    public void setInflater(LayoutInflater i) {
        inflater = i;
    }


    /*** Controllers ***/

    GenerateEvent user;
    private ChangeUserInfoController changeInfoController = null;

    GenerateEvent userInfo;
    private UserInfoController userInfoController = null;

    /*******************/

    private static View view;
    static {
        view = null;
    }

    private EditText status = null;
    private TextView nick = null;
    private Button save = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.screen_user_info_change,
                    container, false);
        }catch (InflateException e) {}

        app = App.getInstance();
        App.now = App.Screen.user_info_change;

        me = this;

        setEvents();

        status = (EditText)view.findViewById(R.id.change_info_status);
        nick = (TextView)view.findViewById(R.id.info_nick);

        save = (Button)view.findViewById(R.id.change_status_button);
        save.setOnClickListener(this);

        //userInfoController = new UserInfoController(getActivity(), userInfo);
        //userInfoController.setPack(app.sid, app.cid, app.cid /* !!! */);
        //userInfoController.startController();

        changeInfoController = new ChangeUserInfoController(getActivity(), user);

        return view;
    }

    private void HandleError(int status) {
        App.Toast(app, "Error!");
    }

    private void setEvents() {
        userInfo = new GenerateEvent(new Handler() {
            public void handleMessage(Message msg) {
                Pack s = Pack.parseMessagePack(msg);
                nick.setText(
                        s.data.get("nick")
                );
            }
        });

        user = new GenerateEvent(new Handler() {
            public void handleMessage(Message msg) {
                Pack s = Pack.parseMessagePack(msg);
                boolean bool = Pack.statusError(s) < 0;
                if(bool) App.Toast(app, "Success!");
                else App.Toast(app, "Try later.");
            }
        });
    }

    @Override
    public void onClick(View v) {
        String value_status = status.getText().toString();
        changeInfoController.setPack(app.sid, app.cid, value_status);
        changeInfoController.startController();
    }
}
