package com.messenger.inwady.messenger.custom.dummy;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DummyContent {

    public static List<DummyItem> ITEMS = new ArrayList<DummyItem>();

    public static void addItem(DummyItem item) {
        ITEMS.add(item);
    }

    public static class DummyItem {
        public Bitmap bitmap = null;

        public String id = null;

        public String name = null;
        public String desc = null;

        public int online = 0;

        private String chid = null;
        public DummyItem setChatId(String chid) {
            this.chid = chid;
            return this;
        }
        public String getChatId() {
            return chid;
        }


        public DummyItem(String id, String name, int online, String desc) {
            this.id = id;
            this.name = name;
            this.desc = desc;
            this.online = online;
        }

        public DummyItem(String id, String name, String desc, int online, Bitmap bitmap) {
            this.id = id;
            this.name = name;
            this.desc = desc;
            this.online = online;
            this.bitmap = bitmap;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
