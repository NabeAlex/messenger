package com.messenger.inwady.messenger.ui;

import android.app.Activity;
import android.widget.EditText;
import android.widget.LinearLayout;

public class DefaultEditText {
    public EditText edit = null;
    public DefaultEditText(Activity act) {
        edit = new EditText(act);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        edit.setLayoutParams(lp);
    }
}
