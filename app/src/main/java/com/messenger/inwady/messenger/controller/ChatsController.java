package com.messenger.inwady.messenger.controller;

import android.app.Activity;
import android.util.Log;

import com.messenger.inwady.messenger.fragment.content.Chats;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;
import com.messenger.inwady.messenger.utils.MD5;

import java.util.HashMap;

public class ChatsController extends MainController {

    public static String action = "channellist";

    public ChatsController(Activity act, GenerateEvent event) {
        super(act, event);
    }

    public void setPack(String sid, String cid) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("sid", sid);
        map.put("cid", cid);
        pack = Pack.genDefaultPack(action, map);
    }

}
