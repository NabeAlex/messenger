package com.messenger.inwady.messenger.controller;

import android.app.Activity;

import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;

import java.util.HashMap;

public class ChangeUserInfoController extends MainController {
    public static String action = "setuserinfo";

    public ChangeUserInfoController(Activity act, GenerateEvent event) {
        super(act, event);
    }

    public void setPack(String sid, String cid, String status) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("sid", sid);
        map.put("cid", cid);
        map.put("user_status", status);
        pack = Pack.genDefaultPack(action, map);
    }
}
