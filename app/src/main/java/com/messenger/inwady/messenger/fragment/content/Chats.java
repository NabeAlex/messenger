package com.messenger.inwady.messenger.fragment.content;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.messenger.inwady.messenger.Main;
import com.messenger.inwady.messenger.R;
import com.messenger.inwady.messenger.controller.ChatsController;
import com.messenger.inwady.messenger.custom.ChatsFragment;
import com.messenger.inwady.messenger.model.App;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Chats extends Fragment implements View.OnClickListener {
    public App app = null;
    public Fragment me = null;

    public LayoutInflater inflater = null;
    public void setInflater(LayoutInflater i) {
        inflater = i;
    }

    public Fragment chats_list;

    /*** Controllers ***/

    GenerateEvent chats;
    private ChatsController chatsController= null;

    /*******************/

    private static View view;
    static {
        view = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.screen_main,
                    container, false);
        }catch (InflateException e) {}

        app = App.getInstance();
        app.updateChat = new UpdateChat();
        ((Main) getActivity()).setToolbar("Messenger");

        me = this;

        App.now = App.Screen.chats_screen;

        setEvents();
        chatsController = new ChatsController(getActivity(), chats);
        chatsController.setPack(app.sid, app.cid);
        chatsController.startController();

        return view;
    }

    class UpdateChat extends Handler {
        public void handleMessage(android.os.Message msg) {
            chatsController.setPack(app.sid, app.cid);
            chatsController.startController();
        }
    }

    private void HandleError(int status) {
        App.Toast(app, "Error!");
    }

    private void setEvents() {
        chats = new GenerateEvent(null);
    }

    @Override
    public void onClick(View v) {

    }
}
