package com.messenger.inwady.messenger.fragment.content;

import android.app.Fragment;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.messenger.inwady.messenger.R;
import com.messenger.inwady.messenger.controller.ChangeUserInfoController;
import com.messenger.inwady.messenger.model.App;

public class Settings extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = null;
        try {
            view = inflater.inflate(R.layout.screen_settings,
                    container, false);
        }catch (InflateException e) {}

        return view;
    }

}
