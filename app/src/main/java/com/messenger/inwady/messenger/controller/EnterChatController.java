package com.messenger.inwady.messenger.controller;


import android.app.Activity;

import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;

import java.util.HashMap;

public class EnterChatController extends MainController {

    public static String action = "enter";

    public EnterChatController(Activity act, GenerateEvent event) {
        super(act, event);
    }

    public void setPack(String sid, String cid, String channel) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("sid", sid);
        map.put("cid", cid);
        map.put("channel", channel);
        pack = Pack.genDefaultPack(action, map);
    }
}
