package com.messenger.inwady.messenger.model;

import android.app.Activity;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.messenger.inwady.messenger.MessageService;
import com.messenger.inwady.messenger.controller.AuthController;
import com.messenger.inwady.messenger.data.Resource;
import com.messenger.inwady.messenger.model.encryption.EncodingString;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.ConnectTask;
import com.messenger.inwady.messenger.model.web.GenerateEvent;
import com.messenger.inwady.messenger.model.web.bind.MessageBinder;
import com.messenger.inwady.messenger.utils.MessengerSharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.Socket;

public class App extends Application {
    private static App app = null;
    public static Router router = new Router();
    public static MessengerSharedPrefs mprefs;

    public static Screen now = null;
    public enum Screen {
        chat, user_info, chats_screen, user_info_change, settings
    }
    public static boolean isMenuByBack() {
        return now == Screen.user_info_change || now == Screen.user_info_change || now == Screen.chat;
    }

    /*** Controllers ***/

    public GenerateEvent lin;
    public GenerateEvent reg;
    public AuthController authController = null;

    /*******************/

    public String sid = null;
    public String cid = null;
    public String login = null;
    public String nickname = null;

    public Handler updateChat = null;

    public void setAuth(String sid, String cid) {
        this.sid = sid;
        this.cid = cid;
    }

    public Socket socket = null;

    public Boolean onSocket = false;

    public void initSocket() {
        ConnectTask connectTask = new ConnectTask();
        connectTask.app = app;
        connectTask.start(null, new HandShake());
    }

    //**** Service's work ****//
    public MessageService mService = null;

    private boolean Bound = false;

    public boolean getWorkService() {
        return  Bound;
    }

    public void setWorkService(boolean status) {
        Bound = status;
    }

    public ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            MessageBinder mb = (MessageBinder) service;
            mService = mb.getService();
            initSocket();
            Bound = true;
        }

        public void onServiceDisconnected(ComponentName className) {
            Bound = false;
            if(app.getWorkService()) {
                app.mService.destroySocket();
                app.setWorkService(false);
            }
            unbindService(mConnection);
        }
    };;

    //************************//

    @Override
    public void onCreate() {
        if (app != null) return;
        else app = this;

        super.onCreate();
        Resource.initResource(this);
        EncodingString.setKey(Resource.SectetKey);

        SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(this);
        mprefs = new MessengerSharedPrefs(p, p.edit());

        Intent intent = new Intent(this, MessageService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static App getInstance(){
        return app;
    }

    private String lastS = null;
    private JSONObject lastJ = null;
    private String lastA = null;

    public class HandShake extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            onSocket = true;

            //mService.setSocket(socket);
            mService.setHandlerReceiver(new Handler() {
                public void handleMessage(Message msg) {
                    lastS = Pack.parseMessageJson(msg);
                    try {
                        lastJ = new JSONObject(lastS);
                        lastA = lastJ.getString("action");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    try {
                        if(Pack.statusError(lastJ.getJSONObject("data")) > 0)
                            mService.initSocketWithService();
                    }catch (Exception e) {}

                    router.packEvent(lastA, lastS);
                    }
                }

                );
                mService.runReceiver();
            }
        }

        public void hideKeyboard(Activity act) {
            InputMethodManager inputMethodManager = (InputMethodManager) this.getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = act.getCurrentFocus();

            if(view == null) {
                view = new View(this);
            }
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        public void SnackBar(CoordinatorLayout c, String text) {
            Snackbar.make(c, text, Snackbar.LENGTH_SHORT).show();
        }

        public void Toast(String text) {
            Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
        }

        public static void Toast(Context act, String text) {
            Toast.makeText(act, text, Toast.LENGTH_SHORT).show();
        }
}
