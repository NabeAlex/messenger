package com.messenger.inwady.messenger.controller;

import android.app.Activity;

import com.messenger.inwady.messenger.model.App;
import com.messenger.inwady.messenger.model.Router;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;

import java.util.HashMap;

public class MainController {
    protected App app = null;
    protected Activity activity;

    public GenerateEvent event = null;

    protected Pack pack = null;

    protected Router link;

    public MainController(Activity act) {
        activity = act;
    }

    public MainController(Activity act, GenerateEvent event) {
        activity = act;
        app = (App)act.getApplication();
        link = Router.getRouter();
        this.event = event;
    }

    public MainController(Activity act, Pack pack) {
        activity = act;
        app = (App)act.getApplication();
        link = Router.getRouter();
        this.pack = pack;
    }

    public MainController(Activity act, GenerateEvent event, Pack pack) {
        activity = act;
        app = (App)act.getApplication();
        link = Router.getRouter();
        this.event = event;
        this.pack = pack;
    }

    public void startController() {
        link.putEvent(pack.action, event.getPost());
        event.getBegin().handleMessage(null);
        app.mService.setHandlerRequest(event.getPre());
        app.mService.runRequest(pack.toJson());
    }

    public void onDestroy() { return; }

    public void pushRouter(String key) {
        App.router.putEvent(key, event.getPost());
    }

    public void setPack(Pack pack) {
        this.pack = pack;
    }

    public Pack getPack() {
        return pack;
    }

    public void setEvent(GenerateEvent e) { event = e; }

    public GenerateEvent getEvent() { return event; }
}
