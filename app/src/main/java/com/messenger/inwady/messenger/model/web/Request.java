package com.messenger.inwady.messenger.model.web;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class Request implements ITask {
    public static PrintWriter pw = null;
    @Override
    public boolean start(Socket s, Handler handler) {
        return false; /* or default value */
    }

    public boolean start(Socket s, Handler handler, String message) {
        if(s == null) return false;
        RequestTask rt = new RequestTask();
        rt.message = message;
        rt.h = handler;
        try {
            if(pw == null) {
                pw = new PrintWriter(s.getOutputStream(), true);
            }
        } catch (IOException e) { e.printStackTrace(); }

        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) rt.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else rt.execute();

        return true;
    }

    @Override
    public void destroyTask() {
        pw = null;
    }

    public class RequestTask extends AsyncTask<Void, Void, Void> {

        public String message;

        public Handler h = null;

        @Override
        protected Void doInBackground(Void... params) {
            pw.write(message);
            pw.flush();
            Log.d("Sent", message);

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if(h != null) h.handleMessage(new Message());
        }

    }

}
