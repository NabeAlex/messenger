package com.messenger.inwady.messenger.model.utils;

import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Pack {
    public String action = null;
    public HashMap<String, String> data = null;

    public  Pack() {
        this.action = "";
        this.data = new HashMap<String, String>();
    }

    public Pack(String json) {
        Pack pack = parseJSON(json);
        this.data = pack.data;
        this.action = pack.action;
    }

    public Pack(String action,HashMap<String, String> data) {
        this.action = action;
        this.data = data;
    }

    public Pack parseJSON(String str) {
        Pack pack = new Pack();
        try {
            JSONObject json = new JSONObject(str);
            pack.action = (String) json.get("action");

            if(pack.action.equals("welcome")) return pack; /* Start point */

            JSONObject attr = json.getJSONObject("data");
            Iterator<?> i = attr.keys();
            String value;
            while(i.hasNext()) {
                value = (String) i.next();
                String tmp = attr.get(value).toString();
                pack.data.put(value, tmp);
            }
        } catch (JSONException e) {
            return null;
        }
        return pack;
    }

    public String toJson() {
        JSONObject json = new JSONObject();
        JSONObject attr = new JSONObject();
        try {
            json.put("action", action);
            json.put("data", attr);

            Iterator i = data.entrySet().iterator();
            Map.Entry value;
            while (i.hasNext()) {
                value = (Map.Entry)i.next();
                attr.put((String) value.getKey(), value.getValue());
                i.remove();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

    public String toString() {
        String s = action + "\n";
        Iterator i = data.entrySet().iterator();
        Map.Entry value;
        while (i.hasNext()) {
            value = (Map.Entry)i.next();
            s += value.getKey() + " : " + value.getKey() + "\n";
            i.remove();
        }
        return s;
    }

    public static Pack genDefaultPack(String action, HashMap<String, String> h) {
        Pack pack = new Pack();
        pack.action = action;
        pack.data = h;
        return pack;
    }

    public static Message genMessageJson(String json) {
        Message m = new Message();
        Bundle bundle = new Bundle();
        bundle.putString("JSON_ANSWER", json);
        m.setData(bundle);
        return m;
    }

    public static String parseMessageJson(Message m) {
        return m.getData().getString("JSON_ANSWER");
    }

    public static Pack parseMessagePack(Message m) {
        return new Pack(parseMessageJson(m));
    }

    public static int statusError(JSONObject data) throws JSONException {
        return data.getInt("status");
    }

    public static int statusError(Pack pack) {
        return Integer.parseInt(pack.data.get("status"));
    }
    public static int statusError(Message pack) { return statusError(parseMessagePack(pack)); }
}
