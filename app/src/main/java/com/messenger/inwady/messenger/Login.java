package com.messenger.inwady.messenger;


import android.app.Activity;
import android.app.FragmentManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;

import com.messenger.inwady.messenger.model.App;
import com.messenger.inwady.messenger.fragment.auth.Authorization;
import com.messenger.inwady.messenger.fragment.auth.Splash;

public class Login extends FragmentActivity {
    public App app = null;
    final Handler handler = new Handler();

    FragmentManager fm = null;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        app = App.getInstance();

        fm = getFragmentManager();
        fm.beginTransaction().replace(R.id.frag, new Splash()).commit();


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                while (!app.onSocket) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                fm.beginTransaction().replace(R.id.frag, new Authorization()).commit();
            }
        }, 2000);
    }

    @Override
    public void onBackPressed()
    {
        if (getFragmentManager().getBackStackEntryCount() > 0){
            getFragmentManager().popBackStack();
        }
        else super.onBackPressed();
    }

    @Override
    public void onConfigurationChanged(Configuration newc) {
        super.onConfigurationChanged(newc);
    }
}

