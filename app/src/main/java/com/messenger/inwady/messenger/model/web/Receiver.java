package com.messenger.inwady.messenger.model.web;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.utils.JSON;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

public class Receiver implements ITask {
    public static int i = 0;

    InputStream inputStream = null;

    private ReceiverTask task = null;
    private Socket socket = null;

    @Override
    public boolean start(Socket s, Handler handler) {
        if(s == null) return false;
        try {
            if(socket == null) socket = s;
            if(inputStream == null) inputStream = s.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        i++;
        task = new ReceiverTask();
        task.h = handler;

        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else task.execute();
        return true;
    }

    public boolean setSocket(Socket s) {
        if(s == null) return false;

        try {
            if(socket == null) {
                socket = s;
            }
            if(inputStream == null) {
                inputStream = s.getInputStream();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public void destroyTask() {
        socket = null;
        inputStream = null;
    }

    public void stopTask() {
        task.WorkNow = false;
    }

    private class ReceiverTask extends AsyncTask<Void, String, Void> {
        public Boolean WorkNow = false;

        public Handler h = null;


        public StringBuilder tmp = null;

        ByteArrayOutputStream receive;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            WorkNow = true;
        }

        protected void onProgressUpdate(String... progress) {
            h.handleMessage(Pack.genMessageJson(progress[0]));
        }

        @Override
        protected Void doInBackground(Void... params) {
            tmp = new StringBuilder();
            receive = new ByteArrayOutputStream();

            int stack = 0;
            byte bit;

            while (true) {
                synchronized (WorkNow) {
                    if(!WorkNow) break;
                }
                if (socket != null && !socket.isClosed()) {
                    try {
                        synchronized (inputStream) {
                            while (inputStream.available() > 0) {
                                bit = (byte) inputStream.read();
                                receive.write((int) bit);
                                if (receive.toString().endsWith("{"))
                                    stack += 1;
                                else if (receive.toString().endsWith("}"))
                                    stack -= 1;
                                if (stack == 0) {

                                    if (receive != null) {
                                        if(JSON.isJSONValid(receive.toString("utf-8"))) {
                                            publishProgress(receive.toString("utf-8"));
                                        }
                                    }
                                    receive.reset();
                                }
                            }
                        }

                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ie) {
                            ie.printStackTrace();
                        }

                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

    }
}
