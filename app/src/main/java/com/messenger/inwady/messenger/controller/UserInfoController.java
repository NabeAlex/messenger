package com.messenger.inwady.messenger.controller;

import android.app.Activity;
import android.os.Message;

import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;

import java.util.HashMap;

public class UserInfoController extends MainController {
    public static String action = "userinfo";

    public UserInfoController(Activity act, GenerateEvent event) {
        super(act, event);
    }

    public void setPack(String sid, String cid, String uid) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("sid", sid);
        map.put("cid", cid);
        map.put("user", uid);
        pack = Pack.genDefaultPack(action, map);
    }
}
