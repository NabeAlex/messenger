package com.messenger.inwady.messenger;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.messenger.inwady.messenger.controller.AddChannelController;
import com.messenger.inwady.messenger.controller.AuthController;
import com.messenger.inwady.messenger.controller.ChatsController;
import com.messenger.inwady.messenger.custom.ChatsFragment;
import com.messenger.inwady.messenger.fragment.auth.Splash;
import com.messenger.inwady.messenger.fragment.content.ChatField;
import com.messenger.inwady.messenger.fragment.content.Chats;
import com.messenger.inwady.messenger.fragment.content.Settings;
import com.messenger.inwady.messenger.fragment.content.UserInfo;
import com.messenger.inwady.messenger.fragment.content.UserInfoChange;
import com.messenger.inwady.messenger.model.App;
import com.messenger.inwady.messenger.model.Router;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;
import com.messenger.inwady.messenger.model.web.bind.MessageBinder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main extends AppCompatActivity {
    public App app = null;

    FragmentManager fm = null;
    private Chats chats;

    private ChatField chatField;
    private UserInfo userInfo;

    ListView drawer = null;
    ArrayAdapter<String> adapter = null;
    List<String> array = null;

    private ActionBarDrawerToggle mDrawerToggle;

    DrawerLayout drawerLayout = null;
    Toolbar toolbar;

    /*** Controllers ***/

    GenerateEvent channel;
    private AddChannelController addChannelController = null;

    /*******************/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        app = App.getInstance();
        App.now = App.Screen.chats_screen;

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        fm = getFragmentManager();
        fm.beginTransaction().replace(R.id.main_frag, new Chats()).commit();

        setMenu();
        drawer = (ListView)findViewById(R.id.left_menu_list);
        adapter = new ArrayAdapter<String>(this, R.layout.menu_list_item, array);
        drawer.setAdapter(adapter);

        drawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView a, View v, int pos, long l) {
                clickMenu(pos + 1);
            }

        });

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        setDrawerState(true);

        setEvents();
        addChannelController = new AddChannelController(this, channel);
        addChannelController.setPack(app.sid, app.cid);
    }

    private void setEvents() {
        channel = new GenerateEvent(new Handler() {
            public void handleMessage(Message msg) {
                if(Pack.statusError(msg) == 0) {
                    addChannelController.dismissAlert();
                    if(app.updateChat != null) app.updateChat.handleMessage(null);
                    //app.Toast("Added a new channel");
                    //updateFragment(App.Screen.chats_screen);
                }else{
                    addChannelController.throwText("Error", Color.RED);
                }
            }
        });
    }

    Fragment f;
    public boolean updateFragment(App.Screen e) {
        boolean root = App.now == App.Screen.chats_screen;
        boolean main = false;

        switch (e) {
            case chats_screen:
                main = true;
                App.now = App.Screen.chats_screen;
                f = new Chats();
                break;
            case user_info_change:
                App.now = App.Screen.user_info_change;
                f = new UserInfoChange();
                break;
            case user_info:
                App.now = App.Screen.user_info;
                f = new UserInfo();
                break;
            case settings:
                App.now = App.Screen.settings;
                f = new Settings();
                break;
            default: return false;
        }
        if(root) {
            fm.beginTransaction()
                    .replace(R.id.main_frag, f).addToBackStack("main")
                    .commit();
        }else {
            fm.beginTransaction()
                    .replace(R.id.main_frag, f).addToBackStack("menu")
                    .commit();
        }
        if(main) {
            setMenuState(R.menu.main);
            setDrawerState(true);
        }
        else {
            setMenuState(R.menu.empty);
            setDrawerState(false);
        }
        return true;
    }

    public boolean updateChatFragment(Fragment fragment) {
        if(fragment instanceof ChatField || fragment instanceof UserInfo) {
            if(fragment instanceof ChatField) {
                chatField = (ChatField)fragment;
                setDrawerState(false);
                setMenuState(R.menu.empty);
            }

            if(fragment instanceof UserInfo) {
                userInfo = (UserInfo) fragment;
            }
            fm.beginTransaction()
                    .replace(R.id.main_frag, fragment)
                    .addToBackStack("chat")
                    .commit();
        } else return false;

        return true;
    }


    @Override
    public void onStop() {
        if(chatField != null) chatField.leaveChat();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed()
    {
        if(App.isMenuByBack()) {
            setDrawerState(true);
            setMenuState(R.menu.main);

            if (App.now == App.Screen.chat)
                chatField.leaveChat();
        }
        if (fm.getBackStackEntryCount() > 0){
            fm.popBackStack();
        }
        else super.onBackPressed();
    }

    private static int menu_;
    static { menu_ = R.menu.main; }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(menu_, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                addChannelController.setAlert(this, getLayoutInflater()).setHandlerAlert()
                                    .showAlert();
                break;
            case R.id.action_logout:
                app.mprefs.clear();

                App.now = null;

                app.mService.initSocketWithService();

                Intent intent = new Intent(this, Login.class);
                intent.setFlags(Intent.FILL_IN_ACTION);
                startActivity(intent);
                finish();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setMenu() {
        array = new ArrayList<String>();
        /* 1 */ array.add(getString(R.string.left_menu_1));
        /* 2 */ array.add(getString(R.string.left_menu_2));
    }

    private  void clickMenu(int index) {
        if(index == 1) {
            if(App.now == App.Screen.settings) return;
            drawerLayout.closeDrawers();
            updateFragment(App.Screen.settings);
        }else if(index == 2) {
            if(App.now == App.Screen.user_info_change) return;
            drawerLayout.closeDrawers();
            updateFragment(App.Screen.user_info_change);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newc) {
        super.onConfigurationChanged(newc);
    }

    public void setDrawerState(boolean isEnable) {
        if (isEnable) {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

            mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                    toolbar, R.string.drawer_open, R.string.drawer_close) {

                public void onDrawerClosed(View view) {
                    super.onDrawerClosed(view);
                    setMenuState(R.menu.main);
                }

                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                    setMenuState(R.menu.empty);
                }
            };


            drawerLayout.setDrawerListener(mDrawerToggle);
            drawerLayout.setScrimColor(Color.TRANSPARENT);

            mDrawerToggle.syncState();

        } else {
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
    }

    public void setMenuState(int i) {
        menu_ = i;
        invalidateOptionsMenu();
    }

    public void setToolbar(String string) {
        toolbar.setTitle(string);
    }
}
