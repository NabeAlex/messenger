package com.messenger.inwady.messenger.controller;

import android.app.Activity;
import android.os.Message;

import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.GenerateEvent;

import java.util.HashMap;

public class LogoutController extends MainController {
    public static String action = "logout";

    public LogoutController(Activity act, GenerateEvent event) {
        super(act, event);
    }

    @Override
    public void startController() {
        event.getBegin().handleMessage(null);
    }
}
