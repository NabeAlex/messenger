package com.messenger.inwady.messenger.ui.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.messenger.inwady.messenger.R;
import com.messenger.inwady.messenger.model.utils.Message;

import java.util.ArrayList;
import java.util.List;


public class MessageArrayAdapter extends ArrayAdapter<Message> {

    private TextView chatText;
    private TextView userText;
    private List<Message> chatMessageList = new ArrayList<Message>();
    private float displayDensity;

    @Override
    public void add(Message object) {
        chatMessageList.add(object);
        super.add(object);
    }

    public MessageArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        displayDensity = context.getResources().getDisplayMetrics().density;
    }

    public int getCount() {
        return this.chatMessageList.size();
    }

    public Message getItem(int index) {
        return this.chatMessageList.get(index);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Message obj = getItem(position);

        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.message_list_item, parent, false);
        }

        TextView chatText;
        TextView userText;
        LinearLayout linearLayout = (LinearLayout) row.findViewById(R.id.singleMessageContainer);;
        LinearLayout linearLayoutMy = (LinearLayout) row.findViewById(R.id.singleMessageContainerMy);;
        if (obj.my) {
            linearLayout.getLayoutParams().height = 0;
            linearLayout.setVisibility(View.INVISIBLE);
            linearLayoutMy.getLayoutParams().height = Math.round(100 * displayDensity);
            linearLayoutMy.setVisibility(View.VISIBLE);
            chatText = (TextView) row.findViewById(R.id.messageMY);
            userText = (TextView) row.findViewById(R.id.uidMY);
        }else{
            linearLayoutMy.getLayoutParams().height = 0;
            linearLayoutMy.setVisibility(View.INVISIBLE);
            linearLayout.getLayoutParams().height = Math.round(100 * displayDensity);
            linearLayout.setVisibility(View.VISIBLE);
            chatText = (TextView) row.findViewById(R.id.message);
            userText = (TextView) row.findViewById(R.id.uid);
        }
        chatText.setText(obj.message);
        userText.setText(obj.user);
        return row;

    }

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

}
