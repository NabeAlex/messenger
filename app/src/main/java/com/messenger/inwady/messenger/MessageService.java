package com.messenger.inwady.messenger;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.messenger.inwady.messenger.model.App;
import com.messenger.inwady.messenger.model.utils.Pack;
import com.messenger.inwady.messenger.model.web.ConnectTask;
import com.messenger.inwady.messenger.model.web.GenerateEvent;
import com.messenger.inwady.messenger.model.web.Receiver;
import com.messenger.inwady.messenger.model.web.Request;
import com.messenger.inwady.messenger.model.web.bind.MessageBinder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;

public class MessageService extends Service {
    private App app;

    private Socket socket = null;

    public void initSocketWithService() {
        app.onSocket = false;
        destroySocket();
        ConnectTask connectTask = new ConnectTask();
        connectTask.app = app;
        connectTask.start(null, new HandShake());
    }

    public void destroySocket() {
        app.socket = null;
        receiver.destroyTask();
        request.destroyTask();
    }

    public class HandShake extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            app.onSocket = true;
            receiver.setSocket(socket);
            if(App.now != null) {
                app.authController.setPack(app.login, app.mprefs.getPassword(), false);
                app.authController.setEvent(new GenerateEvent(new Handler() {
                     public void handleMessage(Message msg) {
                        // OK or TO (AUTH)
                     }
                }));
                app.authController.startController();
            }

        }
    }

    public void setSocket(Socket s) {
        socket = s;
        app.socket = s;
    }

    ///**** Receiver ****///

    private Receiver receiver = null;
    private Handler receiver_handler = null;

    public void setHandlerReceiver(Handler handler) {
        receiver_handler = handler;
    }

    public void runReceiver() {
        if(receiver == null) receiver = new Receiver();
        receiver.start(socket, receiver_handler);
    }

    ///**** Request  ****///

    private Request request = new Request();
    private Handler request_handler;
    private String message = null;

    public void setHandlerRequest(Handler handler) { request_handler = handler; }
    public void setMessage(String m) { message = m; }
    public void runRequest(String message) {
        request.start(socket, request_handler, message);
    }
    ///******************///

    private final IBinder mBinder = new MessageBinder(this);

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        app = (App)getApplication();
        return mBinder;
    }

}
